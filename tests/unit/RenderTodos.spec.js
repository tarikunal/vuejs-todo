import { shallowMount, flushPromises } from '@vue/test-utils'
import axios from 'axios'
import ToDos from '../../src/views/Todos.vue'

const mockToDoList = [
  { id: 1, text: 'buy some milk' }
]


jest.mock('axios', () => ({
  post: jest.fn(() => Promise.resolve({ data: mockToDoList })),
  get: jest.fn(() => Promise.resolve({ data: mockToDoList })),
}));


describe('Todos.vue', () => {
  const wrapper = shallowMount(ToDos);

  it('renders todos text when click', async () => {

    const msg = 'buy some milk';
    const textInput = wrapper.find('input[type="text"]');
    await textInput.setValue(msg);
    await wrapper.find("button").trigger("click");
    await flushPromises();
    const tex = wrapper.findAll('#todo-text');
    expect(tex[0].text()).toContain(msg);
  });
})
