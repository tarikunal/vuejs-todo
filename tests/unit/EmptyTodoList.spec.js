import { shallowMount } from '@vue/test-utils'
import ToDos from '../../src/views/Todos.vue'



describe('Todos.vue', () => {
    const wrapper = shallowMount(ToDos);

    it('renders empty todos list',   () => {
        const empty = wrapper.find('#emptylist');
        expect(empty.exists()).toBe(true);
    });


})
